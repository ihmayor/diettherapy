﻿using NetworkIt;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarListener : MonoBehaviour
{
    public NetworkItClient networkInterface;
    public bool deliverToSelf = false;
    private bool isBarUp = false;
    private int messageCount = 0;
    private float BarTransparencyMax = 0.57f;

    public static BarListener instance;
    private void Start()
    {
        instance = this;
    }

    public void NetworkIt_Message(object m)
    {
        Message message = (Message)m;
        Debug.Log(message.Subject);
        if (message.Subject.Contains("PutDownBar"))
        {
            if (isBarUp)
                StartCoroutine("FadeOutBar");
        }
        if (message.Subject.Contains("PutUpBar"))
        {
            if (!isBarUp)
                StartCoroutine("FadeInBar");
        }

    }

    IEnumerator FadeInBar()
    {
        Material[] materials = GetComponent<Renderer>().materials;
        if (!isBarUp)
        {
            float increase = 0.14f;
            if (materials[0].color.a + increase > 0.57f)
            {
                increase = 0.57f - materials[0].color.a;
            }
            for (float i = materials[0].color.a; i < materials[0].color.a + increase; i += 0.01f)
            {
                foreach (Material mat in materials)
                {
                    if (mat.color.a >= 0.57f)
                        break;
                    mat.color = new Color(mat.color.r, mat.color.g, mat.color.b, mat.color.a + 0.01f);
                }
                if (materials[0].color.a >= 0.57f)
                    break;
                yield return new WaitForSeconds(0.005f);
            }
        }
        if (materials[0].color.a >= 0.57f)
        {
            isBarUp = true;
            SendBarMessage();
        }
    }


    IEnumerator FadeOutBar()
    {
        Material[] materials = GetComponent<Renderer>().materials;
        float decrease = 0.14f;
        if (materials[0].color.a - decrease <= 0)
        {
            decrease = materials[0].color.a;
        }

        if (isBarUp)
        {
            float checkA = materials[0].color.a - decrease;
            for (float i = materials[0].color.a; i >= checkA; i -= 0.01f)
            {
                foreach (Material mat in materials)
                {
                    if (mat.color.a <= 0)
                        break;
                    mat.color = new Color(mat.color.r, mat.color.g, mat.color.b, mat.color.a - 0.01f);
                }
                if (materials[0].color.a <= 0 )
                    break;
                yield return new WaitForSeconds(0.005f);
            }
        }
        if (materials[0].color.a == 0)
        {
            isBarUp = false;
            SendBarMessage();
        }
    }


    public void NetworkIt_Connect(object args)
    {
        SendBarMessage();
    }

    public void SendExitMessage()
    {
        string msgSubject = "Leaving";
        Debug.Log("sending message" + msgSubject);
        Message m = new Message(msgSubject);
        m.DeliverToSelf = deliverToSelf;
        networkInterface.SendMessage(m);
    }


    public void SendBarMessage()
    {
        string msgSubject = "";
        if (isBarUp)
            msgSubject = "BarIsUp";
        else
            msgSubject = "BarIsDown";

        Debug.Log("sending message" + msgSubject);
        Message m = new Message(msgSubject);
        m.DeliverToSelf = deliverToSelf;
        networkInterface.SendMessage(m);
    }


}
