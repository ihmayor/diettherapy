﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugPosition : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        this.GetComponent<Text>().text = "Pos: "+Camera.main.transform.position.y+",Rot: "+Camera.main.transform.rotation.eulerAngles.x;
	}
}
