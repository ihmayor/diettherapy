﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using JW.UI;

public class ListDebugLog : UIContentManager 
{
	public enum MessageLengthEnum
	{
		Short,
		LongForErrors,
		Long
	}

	public int DisplayedMessages = 60;
	public readonly Queue<GameObject> Messages = new Queue<GameObject>();
	public MessageLengthEnum MessageLength = MessageLengthEnum.Long;
	public GameObject MessagePrefab;

	void Awake()
	{
		MessagePrefab.SetActive(false);
	}

	void OnEnable()
	{
		Application.logMessageReceivedThreaded += onLogMessage;
	}

	void OnDisable()
	{
		Application.logMessageReceivedThreaded -= onLogMessage;
	}

	void onLogMessage(string message, string stackTrace, LogType logType)
	{
		bool longMessage = false;
		if (MessageLength == MessageLengthEnum.Short)
			longMessage = false;
		else if (MessageLength == MessageLengthEnum.Long)
			longMessage = true;
		else if (MessageLength == MessageLengthEnum.LongForErrors)
		{
			if (logType == LogType.Warning || logType == LogType.Error || logType == LogType.Exception)
				longMessage = true;
			else
				longMessage = false;
		}

		string logString = generateLogString(message,stackTrace,logType,longMessage);
		updateTextList(logString);
	}

	string generateLogString(string message, string stackTrace, LogType logType, bool longMessage = false)
	{
		string logEntry;

		if (longMessage)
			logEntry = string.Format("{0}: {1}\n{2}"
				,logType,message,stackTrace);
		else //Short message
			logEntry = string.Format("{0}: {1}",logType, message);

		if (logType == LogType.Error || logType == LogType.Exception) //Give errors and Exceptions a red color
			logEntry = "<color=red>" + logEntry + "</color>";
		else if (logType == LogType.Warning) //Give warnings a orange color
			logEntry = "<color=orange>" + logEntry + "</color>";

		return logEntry;
	}

	/// <summary>
	/// Takes the last x messages in the list and displays them
	/// </summary>
	void updateTextList(string newLog)
	{
		//Create the new text row.
		Text newTextRow = CreateItem<Text>(MessagePrefab);
		newTextRow.text = newLog;
		Messages.Enqueue(newTextRow.gameObject);

		newTextRow.gameObject.SetActive(true);

		//Cap the messages at the displayed message count.
		while(Messages.Count > DisplayedMessages)
			Destroy(Messages.Dequeue());
	}
		
}
