﻿using NetworkIt;
using System.Collections;
using UnityEngine;

public class BreatheListener : MonoBehaviour
{
    float TimePassed;
    float TotalY = 0;
    float CountY = 0;
    float AverageY = 0;
    bool setAverage = false;
    float MaxRotX = 340;
    float MinRotX = 0;

    private static int countExhale;

    public float movementThreshold = 0.0005f;

    public static int GetCountExhale() { return countExhale; }
    public static void ResetCount() { countExhale = 0; }

    // Use this for initialization
    void Start()
    {
        TimePassed = Time.time;
    }

    bool IsRotationStraight()
    {
        float checkX = Camera.main.transform.rotation.eulerAngles.x;
        return checkX < 15f || checkX> 335;
    }

    void FixedUpdate()
    {
        if (Time.time - TimePassed > 10)
        {
            foreach(Transform child in transform.GetAllChildren())
            {
                child.gameObject.SetActive(true);
            }
            AverageY = TotalY / CountY;
            setAverage = true;
        }
        else if (!setAverage)
        {
            float rot = Camera.main.transform.rotation.x;
            
            TotalY += Camera.main.transform.position.y;
            CountY++;
        }
        if (setAverage && IsRotationStraight())
        {
            if (Camera.main.transform.position.y >= AverageY + movementThreshold)
            {
                StopAllCoroutines();
                StartCoroutine("InhaleScale");
            }
            else if (Camera.main.transform.position.y <= AverageY - movementThreshold)
            {
                StopAllCoroutines();
                StartCoroutine("ExhaleScale");
            }
            else
            {
                StopAllCoroutines();
                StartCoroutine("ResetScale");
            }
        }
    }


    bool exhale = false;
    IEnumerator ExhaleScale()
    {
        exhale = false;
        countExhale++;
        Transform[] children = transform.GetAllChildren();
        float diff = children[0].transform.localScale.x- 0.001f;
        float org = children[0].transform.localScale.x;
        while (children[children.Length-1].transform.localScale.x > 0.001f)
        {
            float change = Mathf.Lerp(org, 0.001f, 0.03f);
            for(int i = 0; i< children.Length;i++)
            {
                Transform child = children[i];

                child.localScale = new Vector3(change,change,change);
            }
            yield return new WaitForSeconds(0.0005f);
        }
    
    }

    IEnumerator ResetScale()
    {
        exhale = false;
        Transform[] gObj = transform.GetAllChildren();
        float diff = 0.3f - gObj[0].transform.localScale.x;
        while (gObj[0].transform.localScale.x != 0.3f)
        {
            foreach (Transform t in gObj)
            {
                t.localScale += new Vector3(diff/30, diff/30, diff/30);
            }
            yield return new WaitForSeconds(0.5f);
        }
    }


    IEnumerator InhaleScale()
    {
        exhale = true;
        Transform[] gObj = transform.GetAllChildren();
        while (gObj[gObj.Length - 1].transform.localScale.x <= 0.9f && exhale)
        {
            foreach (Transform t in gObj)
            {
                t.localScale += new Vector3(0.01f, 0.01f, 0.01f);
            }
            yield return new WaitForSeconds(0.5f);
        }
    }

}
