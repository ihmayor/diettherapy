﻿/*
	CSVReader by Dock. (24/8/11)
	http://starfruitgames.com
 
	usage: 
	CSVReader.SplitCsvGrid(textString)
 
	returns a 2D string array. 
 
	Drag onto a gameobject for a demo of CSV parsing.
*/

using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class CSVReader : MonoBehaviour
{
    public TextAsset RescueTimeActivityData;
    public TextAsset RescueTimeCategoryData;
    public TextAsset RescueTimeMapping;
    public TextAsset SleepData;
    public TextAsset StepData;

    private static string[,] currentGrid;
    private static string[,] grid1;
    private static string[,] grid2;
    private static string[,] grid3;
    private static string[,] grid4;
    private static string[,] grid5;


    private static int stepGoal = 5000;//Average Human ought to get 6000 steps.
    private static int sleepGoalMin = 8; // 8 hours * 60minutes/hour 
    private static float productivityGoal = 51; //Ball Park


    private static Dictionary<string, string> garbageModels =
        new Dictionary<string, string>() {
            { "Steps","OutsideAssets/GarbageModels/bottlecapprefab" },
            { "Uncategorized","OutsideAssets/GarbageModels/bottle" },
            { "Sleep","OutsideAssets/GarbageModels/milk2" },
            { "Society","OutsideAssets/GarbageModels/PaperCrumple" },
            { "Entertainment","OutsideAssets/GarbageModels/beerbottleprefab" },
            { "Deviantart.com","OutsideAssets/GarbageModels/fishbowl" },
            { "Video","OutsideAssets/GarbageModels/kitchensink" },
            { "d2l","OutsideAssets/GarbageModels/lechemilk" },
            { "tumblr.com","OutsideAssets/GarbageModels/milk1" },
            { "youtube.com","OutsideAssets/GarbageModels/paperbag" } ,
            { "saipaintTool","OutsideAssets/GarbageModels/redcup"}
        };

    private static Dictionary<string, string> dataVidMapping =
    new Dictionary<string, string>() {
            { "Pebbly","rescAct" },
            { "LiveCarRain","sleep" },
            { "Cave","rescCat" },
            { "CityTimeLapse","step" },
            { "Alps","OutsideAssets/GarbageModels/bottlecapprefab" },
    };

    public void Start()
    {
        RescueTimeActivityData = Resources.Load<TextAsset>("OutsideAssets/RescueTimeActivityData");
        RescueTimeCategoryData = Resources.Load<TextAsset>("OutsideAssets/RescueTimeCategoryData");
        RescueTimeMapping = Resources.Load<TextAsset>("OutsideAssets/RescueTimeMapping");
        SleepData = Resources.Load<TextAsset>("OutsideAssets/Sleep");
        StepData = Resources.Load<TextAsset>("OutsideAssets/Step");


        grid1 = SplitCsvGrid(RescueTimeActivityData.text);
        grid2 = SplitCsvGrid(RescueTimeCategoryData.text);
        grid3 = SplitCsvGrid(RescueTimeMapping.text);
        grid4 = SplitCsvGrid(SleepData.text);
        grid5 = SplitCsvGrid(StepData.text);

        currentGrid = grid1;

        StartCoroutine(StartGarbageOnDelay(3.5f));
    }

    private IEnumerator StartGarbageOnDelay(float delay)
    {
        Debug.Log("Garbage Delay");
        Debug.Log(currentGrid.GetUpperBound(0) + "," + currentGrid.GetUpperBound(1));
        yield return new WaitForSeconds(delay);
        Debug.Log("Loading Garbage");
        float row = System.DateTime.Now.Hour;
        int rowLimit = currentGrid.GetUpperBound(1)-1;
        row = row.Remap(0, 24, 0, rowLimit-1);
        int rowInt = Mathf.FloorToInt(row) + 1;
        Debug.Log("Row:"+row);
        Debug.Log("Row Limit: " + rowLimit);
        GarbageCreator creator = GameObject.Find("GarbageSource").GetComponent<GarbageCreator>();
        int convertedResult = 0;
        string[] labelling;
        string[] garbageVals = getRow(rowInt, out labelling);
        for (int i = 0; i < garbageVals.Length; i++)
        {
            string readVal = garbageVals[i];
            string label = labelling[i];
            if (readVal != null && readVal != "-" && readVal != "")
            {

                if (garbageModels.ContainsKey(label))
                {
                    int.TryParse(readVal, out convertedResult);
                    Debug.Log("Converted Value: " + convertedResult);
                    if (convertedResult > 30)
                        creator.CreateGarbage(convertedResult % 20, convertedResult / 20, garbageModels[label], convertedResult);
                    else
                        creator.CreateGarbage(convertedResult, 1, garbageModels[label], convertedResult);
                }
                else
                {
                    Debug.Log("label: " + label);
                }
            }
        }
    }

    //Get Current Vid 
    //Step to timelapse video


    public static void GetNextGarbage(string vidName)
    {//need to implement
    }


    static public string[] GetColumn(int index)
    {
        string[] gridResult = new string[currentGrid.GetUpperBound(1)];
        for (int i = 0; i < currentGrid.GetUpperBound(1) - 1; i++)
        {
            gridResult[i] = currentGrid[index, i];
        }
        return gridResult;
    }

    public static string[] getRow(int index, out string[] labelling)
    {
        int cutOff;
        int offset = 1;
        if (currentGrid == grid1)
        {
            cutOff = 1;
        }
        else if (currentGrid == grid2)
        {
            offset = 3;
            cutOff = 1;
        }
        else
        {
            cutOff = 2;
        }
        int limit = currentGrid.GetUpperBound(0)-1;
        Debug.Log("Column Limit: " + limit);
        string[] gridResult = new string[limit - cutOff];
        labelling = new string[limit - cutOff];
        for (int i = 0; i+offset <limit - cutOff; i++)
        {
            Debug.Log("Read Grid: "+(i+offset) + "," + index);
            Debug.Log(currentGrid[ i + offset,index]);
            gridResult[i] = currentGrid[ i + offset,index];
            labelling[i] = currentGrid [i + offset, 0];
        }

        return gridResult;
    }


    // outputs the content of a 2D array, useful for checking the importer
    static public void DebugOutputGrid(string[,] grid)
    {
        string textOutput = "";
        for (int y = 0; y < grid.GetUpperBound(1); y++)
        {
            for (int x = 0; x < grid.GetUpperBound(0); x++)
            {

                textOutput += grid[x, y];
                textOutput += "|";
            }
            textOutput += "\n";
        }
    }

    // splits a CSV file into a 2D string array
    static public string[,] SplitCsvGrid(string csvText)
    {
        string[] lines = csvText.Split("\n"[0]);

        // finds the max width of row
        int width = 0;
        for (int i = 0; i < lines.Length; i++)
        {
            string[] row = SplitCsvLine(lines[i]);
            width = Mathf.Max(width, row.Length);
        }

        // creates new 2D string grid to output to
        string[,] outputGrid = new string[width + 1, lines.Length + 1];
        for (int y = 0; y < lines.Length; y++)
        {
            string[] row = SplitCsvLine(lines[y]);
            for (int x = 0; x < row.Length; x++)
            {
                outputGrid[x, y] = row[x];

                // This line was to replace "" with " in my output. 
                // Include or edit it as you wish.
                outputGrid[x, y] = outputGrid[x, y].Replace("\"\"", "\"");
            }
        }

        return outputGrid;
    }

    // splits a CSV row 
    static public string[] SplitCsvLine(string line)
    {
        return (from System.Text.RegularExpressions.Match m in System.Text.RegularExpressions.Regex.Matches(line,
        @"(((?<x>(?=[,\r\n]+))|""(?<x>([^""]|"""")+)""|(?<x>[^,\r\n]+)),?)",
        System.Text.RegularExpressions.RegexOptions.ExplicitCapture)
                select m.Groups[1].Value).ToArray();
    }
}