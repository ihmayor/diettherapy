﻿using NetworkIt;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GarbageCreator : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        GameObject Trash = Resources.Load<GameObject>("OutsideAssets/GarbageModels/PaperCrumple");
    }

    void Update()
    {

    }

    //Create Garbage with given Data
    public void CreateGarbage(int amount,int repeat, string type,float scale)
    {
        if (scale > 10)
        {
            scale = scale.Remap(0, 1000, 0.5f, 5.4f);
        }
        GameObject gObjType = Resources.Load<GameObject>(type);
        if (amount <= 0)
            return;

        if (gObjType == null)
        {
            Debug.Log("Error Loading Garbage Model: " + type);
            return;
        }

        //Create First Instance
        GameObject obj = Instantiate(gObjType);
        obj.gameObject.tag = "Garbage";
        obj.transform.localScale = new Vector3(scale, scale, scale);
        if (obj.transform.position.z < 2)
            obj.transform.position += new Vector3(0, 0, 3.2f);
        Ghosting ghost = obj.GetComponent<Ghosting>();
        if (ghost == null)
        {
            ghost = obj.AddComponent<Ghosting>();
            ghost.InvokeFloatAcrossNum(repeat);
        }
        else
        {
            ghost.InvokeFloatAcrossNum(repeat);
        }

        //Create other instances if needed
        for (int i = 0; i < amount; i++)
        {
            obj = Instantiate(gObjType);
            obj.transform.localScale = new Vector3(scale, scale, scale);
            obj.gameObject.tag = "Garbage";
            if (obj.transform.position.z < 2)
                obj.transform.position += new Vector3(0, 0, 3.2f);
            ghost = obj.GetComponent<Ghosting>();
            if (ghost == null)
            {
                ghost = obj.AddComponent<Ghosting>();
            }
            ghost.InvokeFloatAcrossNum(repeat);
        }
    }

    


    //Load Data from Elsewhere
    void NetworkIt_Message(object m)
    {
        Message msg = (Message)m;
        if (msg.Subject.Contains("GarbageFlood"))
        {
            string type = msg.GetField("Type");
            int amount = 0;
            int.TryParse(msg.GetField("Amount"), out amount);
            int scale = 0;
            int.TryParse(msg.GetField("Scale"), out scale);
            float scaledAmount = scale;
            int repeat = 20;//Todo Calculate
            CreateGarbage(amount, repeat, type, scaledAmount.Remap(0, 20, 1.4f, 8.4f));
        }
    }


}
