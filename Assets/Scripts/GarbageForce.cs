﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GarbageForce : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
    }

    void FixedUpdate()
    {
        if (Mathf.Abs(Camera.main.transform.rotation.eulerAngles.z) >= 40)
        {
            GameObject[] gObj = GameObject.FindGameObjectsWithTag("Garbage");
            foreach (GameObject g in gObj)
            {
                if (g.GetComponent<Rigidbody>() == null)
                    g.AddComponent<Rigidbody>();
                Rigidbody rb = g.GetComponent<Rigidbody>();
                rb.mass = 1f;
                rb.useGravity = true;
            }
        }
        else
        {
            GameObject[] gObj = GameObject.FindGameObjectsWithTag("Garbage");
            foreach (GameObject g in gObj)
            {
                if (g.GetComponent<Rigidbody>() != null)
                    Destroy(g.GetComponent<Rigidbody>());
            }
        }
    }
}
