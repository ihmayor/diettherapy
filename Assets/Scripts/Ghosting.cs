﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ghosting : MonoBehaviour {

    private float floatSpeed = 0.008f;
    private Vector3 target;
    private bool move;
    private bool fadeOut;
    // Use this for initialization
	void Start () {
        Material[] materials = this.GetComponent<Renderer>().materials;
        target = transform.position;
        foreach (Material material in materials)
        {
            Color c = material.color;
            material.color = new Color(c.r, c.g, c.b, 0);
        }

        Transform[] children = transform.GetAllChildren();
        foreach (Transform child in children)
        {
            if (child.GetComponent<Renderer>() != null)
            {

                Material[] materials2 = this.GetComponent<Renderer>().materials;
                foreach (Material material in materials2)
                {
                    Color c = material.color;
                    material.color = new Color(c.r, c.g, c.b, 0);
                }

            }
        }
    }

    public void InvokeFloatAcrossNum(int numTimes)
    {
        for (int i = 0; i < numTimes; i++)
        {
           Invoke("FloatObjectAcross", UnityEngine.Random.Range(numTimes*5f, numTimes * 15f));
       }
    }

    void FloatObjectAcross()
    {
        //Set start position to left side of 'screen'
        Vector3 rotated = Camera.main.transform.rotation * (Vector3.forward * 3f - (Vector3.right * 2.2f));
        transform.position = Camera.main.transform.position + rotated + new Vector3(0, UnityEngine.Random.Range(-2f, 3f));
        //Clear any old falling applied
        if (this.GetComponent<Rigidbody>() != null)
            Destroy(this.GetComponent<Rigidbody>());

        //Fade Object into view
        if (!fadeOut)
            StopAllCoroutines();
        StartCoroutine(FadeObjectIn(2.7f, 0.05f));
       StartCoroutine(FadeObjectOut(10f, 0));
  //      Set up object to right side of 'screen'
        target = Camera.main.transform.position + new Vector3(2.2f, 0, 3.4f);
        target = Camera.main.transform.rotation * target;
        move = true;
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        if (move)
            transform.position = Vector3.Lerp(transform.position, target, floatSpeed);

        if (Math.Round(transform.position.x,2) == Math.Round(target.x,2) && move)
        {
            move = false;
        }
        
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (!fadeOut)
                StopAllCoroutines();
            StartCoroutine(FadeObjectIn(20, 0.08f));
            StartCoroutine(FadeObjectOut(5, 0.98f));
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            FloatObjectAcross();
        }
    }


    IEnumerator FadeObjectIn(float delay,float speed)
    {
        yield return new WaitForSeconds(delay);
        Material[] materials = GetComponent<Renderer>().materials;
        while (materials[materials.Length-1].color.a < 1)
        {
            foreach (Material material in materials)
            {
                float valDiff = 0.1f;
                if (material.color.a + 0.1f > 1)
                    valDiff = material.color.a;
                material.color = new Color(material.color.r, material.color.g, material.color.b, material.color.a + (valDiff));
            }
            yield return new WaitForSeconds(speed);
        }
    }

    IEnumerator FadeObjectOut(float delay, float speed)
    {
        fadeOut = true;
        yield return new WaitForSeconds(delay);
        Material[] materials = GetComponent<Renderer>().materials;
        while (materials[materials.Length-1].color.a > 0)
        {
            foreach(Material material in materials)
            {
                float valDiff = 0.1f;
                if (material.color.a - 0.1f < 0)
                    valDiff = material.color.a;
                material.color = new Color(material.color.r, material.color.g, material.color.b, material.color.a - (valDiff));
            }
            yield return new WaitForSeconds(speed);
        }
        fadeOut = false;
    }


}
