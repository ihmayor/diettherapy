﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hover : MonoBehaviour {

    private bool up;
    private Vector3 upPos;
    private Vector3 downPos;
    // Use this for initialization
    void Start () {
        upPos = transform.position + Vector3.up *0.25f;
        downPos = transform.position + Vector3.down* 0.25f;
	}
	
	// Update is called once per frame
	void Update () {
        if (up)
            transform.position = Vector3.Lerp(transform.position, upPos, 0.05f);
        else
            transform.position = Vector3.Lerp(transform.position, downPos, 0.05f);
        if (transform.position == upPos || transform.position == downPos)
        {
            up = !up;
        }

    }
}
