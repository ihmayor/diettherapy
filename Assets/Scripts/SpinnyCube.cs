﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NetworkIt;


public class SpinnyCube : MonoBehaviour {

    public NetworkItClient networkInterface;
	
    // Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}


    public void NetworkIt_Message(object m)
    {
        Message msg = (Message)m;
        transform.position += new Vector3(1.0f, 0, 0);
    }

    

    public void SendNewPoke()
    {
        if (networkInterface == null)
        {
            Debug.Log("AAAAAAAAA");
            return;
        }


        Debug.Log("newPoke");
        Message m = new Message("Poke!");
        m.DeliverToSelf = true;
        m.AddField("num1", "" + 13); // Cast integers 
        m.AddField("Hello", "HappyHalloween");
        networkInterface.SendMessage(m);
    }


}
