﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoChanger : MonoBehaviour
{
    private float fadePerSecond = 0.1f;

    Vector3 oldRotate;
    float TimePassed;
    GameObject vidPlayer;
    public GameObject loadingObj;
    public Animator anim;
    private float timeWait = 20;
    private int focus = -1;
    private Quaternion MainOrientation;

    public GameObject filterObj;

    // Use this for initialization
    void Start()
    {
        vidPlayer = GameObject.Find("360 Video Player");
        StartCoroutine("FadingStart", "OutsideAssets/Videos/PondDucks");
        TimePassed = Time.time;
        MainOrientation = Camera.main.transform.rotation;
    }

    void FixedUpdate()
    {
        float y1 = Camera.main.transform.rotation.eulerAngles.y;
        float y2 = MainOrientation.eulerAngles.y;
        float diff = 0;
        if (y1 >= y2)
        {
            diff = y1 - y2;
        }
        else
        {
            diff = y2 - y1;
        }

        if (diff >= 270)
        {
            if (!filterObj.activeSelf)
                filterObj.SetActive(true);
            filterObj.GetComponent<Renderer>().material.color = new Color((0.5f), 0.9f, 0.7f, (Time.time - TimePassed) / timeWait);
            if (focus == -1)
            {
                focus = 0;
            }

            if (focus != 0)
            {
                TimePassed = Time.time;
                focus = 0;
            }
            if ((Time.time - TimePassed) > timeWait)
            {
                MainOrientation = Camera.main.transform.rotation;
                CSVReader.GetNextGarbage("LiveCarRain");
                StopAllCoroutines();
                TimePassed = Time.time;
                StartCoroutine("Fading", "OutsideAssets/Videos/LiveCarRain");
                focus = -1;
            }
        }
        else if (diff >= 170)
        {
            if (!filterObj.activeSelf)
                filterObj.SetActive(true);
            filterObj.GetComponent<Renderer>().material.color = Color.cyan;
            filterObj.GetComponent<Renderer>().material.color = new Color((0.8f), 0.98f, 0.91f, (Time.time - TimePassed) / timeWait);
            if (focus == -1)
            {
                focus = 2;
            }
            if (focus != 2)
            {
                TimePassed = Time.time;
                focus = 2;
            }
            if ((Time.time - TimePassed) > timeWait)
            {

                TimePassed = Time.time;
                CSVReader.GetNextGarbage("CityTimeLapse");
                MainOrientation = Camera.main.transform.rotation;
                StartCoroutine("Fading", "OutsideAssets/Videos/CityTimelapse");
                focus = -1;
            }
        }

        else if (diff >= 100)
        {
            if (!filterObj.activeSelf)
                filterObj.SetActive(true);
            filterObj.GetComponent<Renderer>().material.color = new Color(161 / 255, 152 / 255, 197 / 255, (Time.time - TimePassed) / timeWait);
            if (focus == -1)
            {
                focus = 3;
            }
            if (focus != 3)
            {
                TimePassed = Time.time;
                focus = 3;
            }
            if ((Time.time - TimePassed) > timeWait)
            {
                StopAllCoroutines();
                CSVReader.GetNextGarbage("Cave");
                TimePassed = Time.time;
                MainOrientation = Camera.main.transform.rotation;
                StartCoroutine("Fading", "OutsideAssets/Videos/Cave");
                focus = -1;
            }
        }
        else if (diff >= 40)
        {
            if (!filterObj.activeSelf)
                filterObj.SetActive(true);
            filterObj.GetComponent<Renderer>().material.color = new Color((0.1f), 0.5f, 0.2f, (Time.time - TimePassed) / timeWait);
            if (focus == -1)
            {
                focus = 1;
            }
            if (focus != 1)
            {
                TimePassed = Time.time;
                focus = 1;
            }
            if ((Time.time - TimePassed) > timeWait)
            {
                StopAllCoroutines();
                CSVReader.GetNextGarbage("Pebbly");
                TimePassed = Time.time;
                MainOrientation = Camera.main.transform.rotation;
                StartCoroutine("Fading", "OutsideAssets/Videos/Pebbly");
                focus = -1;
            }
        }

        else if (Camera.main.transform.rotation.x < 300 && Camera.main.transform.rotation.x > 20)
        {
            if ((Time.time - TimePassed) > timeWait * 2)
            {
                StopAllCoroutines();
                StartCoroutine("FadingExit");
            }
        }
        else
        {
            if (filterObj.activeSelf && fadingComplete)
                filterObj.SetActive(false);
            if (focus == -1)
            {
                focus = 5;
            }

            if (focus != 5)
            {
                TimePassed = Time.time;
                focus = 5;
            }
            if ((Time.time - TimePassed) > timeWait)
            {
                StopAllCoroutines();
                TimePassed = Time.time;
                focus = -1;
                MainOrientation = Camera.main.transform.rotation;
                if (BreatheListener.GetCountExhale()> 3)
                {
                    AudioClip clip = Resources.Load<AudioClip>("OutsideAssets/bell");
                    GetComponent<AudioSource>().PlayOneShot(clip);
                    BreatheListener.ResetCount();
                }
            }
        }
    }


    IEnumerator FadingExit()
    {
        var material = loadingObj.GetComponent<Renderer>().material;
        var color = material.color;
        material.color = new Color(color.r, color.g, color.b, 0);
        AudioSource aud = vidPlayer.GetComponent<AudioSource>();
        VideoPlayer vid = vidPlayer.GetComponent<VideoPlayer>();
        while (material.color.a < 1)
        {
            if (color.a < 1)
                aud.volume = color.a - (fadePerSecond);
            color = material.color;
            material.color = new Color(color.r, color.g, color.b, color.a + (fadePerSecond));
            material = loadingObj.GetComponent<Renderer>().material;
            yield return new WaitForSeconds(0.05f);
        }
        vid.Stop();
        BarListener.instance.SendExitMessage();
        yield return new WaitForSeconds(30);
    }

    bool fadingComplete = true;
    IEnumerator Fading(string vidLocation)
    {
        fadingComplete = false;
        VideoPlayer vid = vidPlayer.GetComponent<VideoPlayer>();
        AudioSource aud = vidPlayer.GetComponent<AudioSource>();
        var material = loadingObj.GetComponent<Renderer>().material;
        var color = material.color;
        material.color = new Color(color.r, color.g, color.b, 0);
        while (material.color.a < 1)
        {
            color = material.color;
            if (color.a < 1)
                aud.volume = color.a - (fadePerSecond);
            material.color = new Color(color.r, color.g, color.b, color.a + (fadePerSecond));
            material = loadingObj.GetComponent<Renderer>().material;
            yield return new WaitForSeconds(0.05f);
        }
        MainOrientation = Camera.main.transform.rotation;
        VideoClip loaded = Resources.Load<VideoClip>(vidLocation);
        vid.clip = loaded;
        yield return new WaitForSeconds(1.5f);
        fadingComplete = true;
        while (material.color.a > 0)
        {
            color = material.color;
            float valDiff = fadePerSecond;
            if (color.a - fadePerSecond < 0)
                valDiff = color.a;
            if (color.a >= 0)
                aud.volume = 1 - color.a;
            material.color = new Color(color.r, color.g, color.b, color.a - (valDiff));
            material = loadingObj.GetComponent<Renderer>().material;
            yield return new WaitForSeconds(0.05f);
        }
    }



    IEnumerator FadingStart(string vidLocation)
    {

        VideoPlayer vid = vidPlayer.GetComponent<VideoPlayer>();
        AudioSource aud = vidPlayer.GetComponent<AudioSource>();
        vid.Pause();
        VideoClip loaded = Resources.Load<VideoClip>(vidLocation);
        vid.clip = loaded;
        vid.Play();
        yield return new WaitForSeconds(2.5f);
        var material = loadingObj.GetComponent<Renderer>().material;
        while (material.color.a > 0)
        {
            float valDiff = fadePerSecond;
            if (material.color.a - fadePerSecond < 0)
                valDiff = material.color.a;
            if (material.color.a > 0)
                aud.volume = 1 - material.color.a;
            material.color = new Color(material.color.r, material.color.g, material.color.b, material.color.a - (valDiff));
            yield return new WaitForSeconds(0.05f);
        }
    }
}
