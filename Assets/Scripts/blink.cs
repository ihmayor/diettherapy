﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class blink : MonoBehaviour
{

    public MeshRenderer mr;



    // Use this for initialization


    private Vector3 oldValue;

    private void FixedUpdate()
    {
        if (oldValue == null)
        {
            oldValue = Camera.main.transform.rotation.eulerAngles;
        }
        else
        {
            Vector3 diff = Camera.main.transform.rotation.eulerAngles - oldValue; //Theta-2 - theta-1
            Vector3 rotAccl = diff / Time.fixedDeltaTime;
            Debug.Log(rotAccl);
            oldValue = Camera.main.transform.rotation.eulerAngles;
        }
       
    }
    void Start()
    {
        //Input.accelertion(x,y,z) 
         

        //How fast did I turn vs. above how much did i move in general 
        //Rotational Acceleration (use FixedUPdate())
            //Time.fixedDeltaTime
            //read gameObject.transform.rotation
            //How much angle has changed over time

        mr = GetComponent<MeshRenderer>();
        StartCoroutine("BlinkColor");

    }

    // Update is called once per frame
    void Update()
    {
    //    Debug.Log(Camera.main.transform.rotation);
    }

    IEnumerator BlinkColor()
    {
        while (true)
        {
            Debug.Log("Here");
            mr.material.color = Color.yellow;
            yield return new WaitForSeconds(1.0f);
            mr.material.color = Color.white;
            yield return new WaitForSeconds(1.0f);
        }
    }

}
