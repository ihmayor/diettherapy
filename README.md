# Tilt-eD README


*CSVREADER BASE CODE: http://wiki.unity3d.com/index.php?title=CSVReader

*Source for orb: MagicParticlesLite from Asset Store

*Source code for NetworkIt: github.com/kevinta893

##Garbage Models
*["Beer Bottle"](https://3dwarehouse.sketchup.com/model/8c0899da894b41adfaaa4370fa1ccec/Beer-Bottle)
*["Toilet Paper"]( https://3dwarehouse.sketchup.com/model/bb5be0db-e97b-4d6e-ae54-c285f0fe8a73/Toilet-Paper)
*["Jail+Bars"]( https://3dwarehouse.sketchup.com/model/44b5493eec96b9c677f95ed590eec3fc/Jail-Window)
*["Milk Jug"]( https://3dwarehouse.sketchup.com/model/a9cad3a279928c1d8a6ec8c6f11e4804/milk-jug)
*["Leche Milk"]( https://3dwarehouse.sketchup.com/model/6529ba20d01717aa376a7587d31c279b/Leche-Milk)
*["Red Cup"]( https://3dwarehouse.sketchup.com/model/fbaf5d09f33f535d4f42eadbd9a0b820/Red-Plastic-Cup)	
*["Paper Bag"]( https://3dwarehouse.sketchup.com/model/u6fcd385e-7198-45ac-8e5e-79619683b01e/paper-bag)
*["Bottle"]( https://3dwarehouse.sketchup.com/model/cf6843231592b29e2586fa0d0359af6/Bottle)
*["More Paper"]( https://3dwarehouse.sketchup.com/model/df3aa1b3a4b14dc3f97bb0df23fdf026/crumpled-ball-of-paper)
*["PaperCrumple"]( https://3dwarehouse.sketchup.com/model/92c7f4a59cfa7e575e53373709686184/Crumpled-pieceof-paper)
*["fishbowl"]( https://3dwarehouse.sketchup.com/model/7210ec025d5cc1f442c28aee4f9d8a89/Fishbowl)
*["kitchen sink"]( https://3dwarehouse.sketchup.com/model/4f180f0a99cdb2bdc5d1eba75162ba/Kitchen-Sink)
*["Crystal Waters by Purple Music Planet"]( http://www.purple-planet.com/relaxing-music-home/4588759002)


##HOW TO RUN UNITY APPLICATION:
1. Extract Folder 
2. Open Folder using Unity 5.6.4f1. For 2017 or later versions of unity, all XR. namespace references must changed to .VR
3. Select "Diet" Scene
4. Select Android.
5. Player Settings: Virtual Reality Supported. Api Compatibility level: .Net 2.0 Minimum API level Android 4.4. Maximum (Needed Maximum) Android 7.0 OR Nougat API Level 24
6. Press Play ( OR Build and Run/Build APK to install to phone)  

##HOW TO RUN WPF SECONDARY APPLICATION:
_this step is completelly optional_
1. Open BarButton.sln in Visual Studio 2013 or later.
2. Press Play.
3. Set system such that this is the primary application open/focused upon when you enter vr so when you press random butons
you don't have to worry about doing something wierd on your computer. 
